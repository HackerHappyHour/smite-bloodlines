# Bloodlines for Smite

I often recognize the names of other gamers I wind up in ranked queue's with, and
I try to remember what god they played and such, so I can cater my pics/playstyle to those of my team-mates.

The trouble is, existing sites and apps make it difficult for me to find the shared
match history of myself and the other player, and I often can't find the data i need
in time to make the most informed selection and build, because I have to surf through
weeks of my own match history one by one.

Bloodlines for Smite looks to change this, by using social graph technology to
quickly show you how you are connected to the players you are queuing with,
enabling you to make choices that synergize with the playstyles of your team
mates.. even those you may have only just met!