const HiRezCredentials = require('./.credentials.json')
const selectPlayer = require('./lib/selectPlayer')()
const API = require('hirez.js')

var api = new API(HiRezCredentials)

api.smite('XBOX').session.generate()
  .then((res) => {
	api.smite('XBOX').getPlayer(selectPlayer.gamertag)
	  .then(player => console.log(player))
  })
