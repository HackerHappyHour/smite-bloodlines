FROM kkarczmarczyk/node-yarn

WORKDIR /bloodlines

VOLUME /bloodlines

RUN yarn

EXPOSE 3131

ENTRYPOINT ["yarn", "dev"]

CMD ["yarn"]
