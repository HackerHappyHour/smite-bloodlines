const jsdom = require("jsdom")
const $ = require("cheerio")
const { JSDOM } = jsdom


module.exports = function loadMatches(player){

  return JSDOM
    .fromURL(`https://smite.guru/profile/${player.platform}/${player.gamertag}/matches`)
	.then(dom => {
	   return $('.match-widget', dom.serialize())
	})

}
